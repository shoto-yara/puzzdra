<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/puzzdra/data/db_info.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/header.php'); 
	
	$list_id=$_GET["list_id"];
	$category=$_GET["category"];
	
	if(preg_match("/[^0-9]/",$list_id)){
		print '不正な値が入力されています<br>';
		print '<a href="/puzzdra/dungeon/dungeonl_rank.php">ここをクリックしてダンジョントップに戻ってください</a><br>';
	}
	
	$sql="select * from dungeon_data where dungeon_list_id=$list_id and category=$category";
	$dungeon = $dbh->prepare($sql);
	$dungeon->execute();
	?>
	
	<a href="dungeon_list.php?category=<?=$category;?>">戻る</a><br><br>
	<main>
		<?php
		foreach($dungeon as $dungeon_list){
			echo '<a href="/puzzdra/dungeon/dungeon.php?id='.$dungeon_list['dungeon_id'].'">';
			echo $dungeon_list['name'].'<br>スタミナ：'.$dungeon_list['stamina'].'バトル：'.$dungeon_list['battle'].'</a><br><br>';
		} 
		?>
	</main>
	
	<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/footer.php');?>
<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/puzzdra/data/db_info.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/header.php'); 
	
	/******** 変数代入 ********/
	$id=isset($_GET["id"]) ? htmlspecialchars($_GET["id"]) : null;
	$mail_id=isset($_GET["mail_id"]) ? htmlspecialchars($_GET["mail_id"]) : null;
	$delete_flg=isset($_GET["delete_flg"]) ? htmlspecialchars($_GET["delete_flg"]) : null;

	
	if($id!=null){
	echo '<a href="friend_list.php">戻る</a><br><br>';
	}
	else if($mail_id!=null)
	{
	echo '<a href="friend_approval.php">戻る</a><br><br>';
	}
?>
		<main>
		<?php 
		if($id!=null){
			$sql="SELECT * FROM users WHERE id =$id";
			$stmt = $dbh->prepare($sql);
			$stmt->execute();
			
			foreach($stmt as $friend_status){
					echo $friend_status['id'].'  : '.$friend_status['name'].'<br><br>';
					echo '<a href="friend_list.php">戻る</a><br><br>';
			}
		}
		else if($mail_id!=null)
		{
			$sql="
			SELECT
				mail.id,
				mail.user_id,
				mail.friend_id,
				users.name,
				mail.message,
				mail.friend_request
			FROM 
				mail
			INNER JOIN users on mail.friend_id=users.id
				where mail.id=$mail_id
			";
			$stmt = $dbh->prepare($sql);
			$stmt->execute();
			
			if($delete_flg==1)
			{
				$sql = "DELETE from mail where id=$mail_id";
				$delete = $dbh->prepare($sql);
				$delete->execute();
				
				echo '<a href="friend_approval.php">OK</a><br><br>';
			}
			else{
				foreach($stmt as $mail){
					echo "{$mail['id']}  : {$mail['name']}<br><br>";
					echo "{$mail['message']}<br><br>";
					echo '<a href="friend.php?mail_id='.$mail_id.'&delete_flg=1">消去</a><br><br>';
					echo '<a href="friend_approval.php">OK</a><br><br>';
				}
			}
		}
			
		?>
		</main>
		
	<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/footer.php');?>
		
</body>
</html>
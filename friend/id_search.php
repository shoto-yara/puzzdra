<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/puzzdra/data/db_info.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/header.php'); 
	
	$id = isset($_POST["id"]) ? intval($_POST["id"]) : null;
	
	
	echo '<a href="friend_menu.php">戻る</a><br><br>';
	echo '<main>';
		if($id != null)
		{
			if(preg_match("/^[0-9]+$/", $id))
			{
				$sql="SELECT * FROM users WHERE id =$id";
				$stmt = $dbh->prepare($sql);
				$stmt->execute();
				$friend_status = $stmt->fetch(PDO::FETCH_ASSOC);

				if($friend_status!=false){
					print 'このユーザーにフレンド申請を行いますか？<br>';
					echo "{$friend_status['id']}  : {$friend_status['name']}<br>";
					echo '<a href="friend_request.php?id='.$id.'">はい</a><a href="id_search.php">いいえ</a>';
				}
				else{
					print 'エラー<br>';
					print 'ユーザー未登録です。<br><br>';
					print '<a href="id_search.php">OK</a>';
				}
			}
			else
			{
				print 'このIDは検索できません<br>';
				print '<a href="id_search.php">OK</a>';
			}
		}
		else
		{
			echo '<form method="post" action="id_search.php">';
				echo '<input type="text" name="id">';
				echo '<input type="submit" value="検索">';
			echo '</form>';
		}
	echo '</main>';
require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/footer.php');
?>
<?php 
	require_once($_SERVER['DOCUMENT_ROOT'] . "/puzzdra/data/db_info.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/header.php'); 
	
	$mail_id=isset($_GET["mail_id"]) ? intval($_GET["mail_id"]) : null;
	$friend_id=isset($_GET["friend_id"]) ? intval($_GET["friend_id"]) : null;
	$veto=isset($_GET["veto"]) ? intval($_GET["veto"]) : null;

	if(empty($mail_id)==false)
	{
		$sql="
				SELECT 
						* 
				FROM
						friend_list
				WHERE
						friend_list.user_id_1 =$user_id
						AND friend_list.user_id_2=$friend_id
						OR friend_list.user_id_1 =$friend_id
						AND friend_list.user_id_2=$user_id
						";
		$friend = $dbh->prepare($sql);
		$friend->execute();
		
		$friend = $friend->fetch(PDO::FETCH_ASSOC);
	}
	else{
		$sql="
				SELECT
					mail.id,
					mail.user_id,
					mail.friend_id,
					users.name,
					mail.message,
					mail.friend_request
				FROM 
					mail
				INNER JOIN
					users on mail.friend_id=users.id
				WHERE 
					mail.friend_id=$user_id
					OR mail.friend_id=$user_id
					AND mail.friend_request=1
				";
		$mail = $dbh->prepare($sql);
		$mail->execute();
	}
		
?>
	<a href="friend_menu.php">戻る</a><br><br>
	<main>
		<?php 
		if(empty($mail_id)==false)
		{
			if($veto!=1){
				if($friend==false){
					$sql = 'INSERT INTO friend_list(user_id_1, user_id_2) VALUES(?,?)';
					$insert = $dbh->prepare($sql);
					$date[] = $user_id;
					$date[] = $friend_id;
					$insert->execute($date);
				}
				
				else
				{
					echo '友達です。';
				}
			}
			$sql = "DELETE from mail where id=$mail_id";
			$delete = $dbh->prepare($sql);
			$delete->execute();
			
			echo '<a href="friend_approval.php">OK</a><br><br>';
		}
		else{
			foreach($mail as $mail_list){
				if($mail_list['friend_request']==1){
					echo "{$mail_list['friend_id']}.{$mail_list['name']}<br>";
					echo '<a href="friend_approval.php?mail_id='.$mail_list['id'].'&friend_id='.$mail_list['friend_id'].'&veto=0">-容認</a><br><br>';
					echo '<a href="friend_approval.php?mail_id='.$mail_list['id'].'&friend_id='.$mail_list['friend_id'].'&veto=1">-拒否</a><br><br>';
				}
				else
				{
					echo '<a href="friend.php?mail_id='.$mail_list['id'].'">';
					echo "{$mail_list['name']}</a><br><br>";
				}
				
			}
		}
		?>
	</main>
		
	<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/puzzdra/footer.php');?>
	